#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

%:
	dh $@

export VERSION=1.8.3
define BANNER
/*!
 * Autolinker.js
 * $(VERSION)
 *
 * Copyright(c) 2018 Gregory Jacobs <greg@greg-jacobs.com>
 * MIT License
 *
 * https://github.com/gregjacobs/Autolinker.js
 */
endef
export BANNER

export SOURCE=src/Autolinker.js \
	src/Util.js \
	src/HtmlTag.js \
	src/RegexLib.js \
	src/AnchorTagBuilder.js \
	src/htmlParser/HtmlParser.js \
	src/htmlParser/HtmlNode.js \
	src/htmlParser/CommentNode.js \
	src/htmlParser/ElementNode.js \
	src/htmlParser/EntityNode.js \
	src/htmlParser/TextNode.js \
	src/match/Match.js \
	src/match/Email.js \
	src/match/Hashtag.js \
	src/match/Phone.js \
	src/match/Mention.js \
	src/match/Url.js \
	src/matcher/TldRegex.js \
	src/matcher/Matcher.js \
	src/matcher/Email.js \
	src/matcher/Hashtag.js \
	src/matcher/Phone.js \
	src/matcher/Mention.js \
	src/matcher/Url.js \
	src/matcher/UrlMatchValidator.js \
	src/truncate/TruncateEnd.js \
	src/truncate/TruncateMiddle.js \
	src/truncate/TruncateSmart.js

override_dh_auto_build:
	mkdir -p dist

	echo -n 'Autolinker.tldRegex = /(?:' > src/matcher/TldRegex.js
	cat debian/tlds-alpha-by-domain.txt \
	| sed '1d' \
	| tr '[:upper:]' '[:lower:]' \
	| while read line; do echo $$line; if [ "${line:0:4}" = "xn--" ]; then node -e "console.log(require('punycode').toUnicode('$$line'));"; fi; done \
	| sort -u \
	| tr '\n' '|' \
	>> src/matcher/TldRegex.js
	echo ')/;' >> src/matcher/TldRegex.js

	echo "$$BANNER" > dist/Autolinker.js
	cat $$SOURCE \
	| sed "s/\/\* @echo VERSION \*\//$$VERSION/g" - \
	| umd Autolinker \
	>> dist/Autolinker.js

	sed '/\/\/ @if DEBUG/,/\/\/ @endif/d' dist/Autolinker.js \
	| uglifyjs \
	> dist/Autolinker.min.js
